package com.example.hoseein_ramshin

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class WeatherApp : Application() {
}