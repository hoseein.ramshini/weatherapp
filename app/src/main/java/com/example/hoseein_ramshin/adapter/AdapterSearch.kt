package com.example.hoseein_ramshin.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.hoseein_ramshin.data.model.SearchItem
import com.example.hoseein_ramshin.databinding.SimpleSearchBinding

class AdapterSearch
    (
    val items: List<SearchItem>,
    val onItemClick: OnItemClick
) : RecyclerView.Adapter<AdapterSearch.TimeViewHolder>() {


    interface OnItemClick {
        fun itemClick(searchItem: SearchItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimeViewHolder {
        val binding =
            SimpleSearchBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return TimeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TimeViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class TimeViewHolder(private val binding: SimpleSearchBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SimpleDateFormat")
        fun bind(position: Int) {
            val currentItem = items[position]

            binding.name.text = currentItem.title

            binding.name.setOnClickListener {
                onItemClick.itemClick(currentItem)
            }
        }

    }

}