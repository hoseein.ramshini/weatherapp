package com.example.hoseein_ramshin.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.hoseein_ramshin.R
import com.example.hoseein_ramshin.data.model.ListItem
import com.example.hoseein_ramshin.databinding.SimpleItemBinding
import com.example.hoseein_ramshin.util.Constants.DATE_FORMAT
import com.example.hoseein_ramshin.util.Constants.THEME_MODE
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.Calendar

class AdapterTimes
    (
    val items: List<ListItem>,
) : RecyclerView.Adapter<AdapterTimes.TimeViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimeViewHolder {
        val binding = SimpleItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return TimeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TimeViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class TimeViewHolder(val binding: SimpleItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SimpleDateFormat", "SetTextI18n")
        fun bind(position: Int) {
            val currentItem = items[position]

            val calendar = Calendar.getInstance()
            val date = DATE_FORMAT.parse(currentItem.dt_txt)
            if (date != null) {
                calendar.time = date
            }
            if (calendar.get(Calendar.AM_PM) == 1)
                binding.textView2.text = "${calendar.get(Calendar.HOUR)}am"
            else
                binding.textView2.text = "${calendar.get(Calendar.HOUR)}pm"


            val temp=currentItem.main.temp-273.15
            binding.textView.text = "${temp.toInt()}°"


            if (THEME_MODE=="Dark")
            {
                when (currentItem.weather[0].icon) {
                    "01n" -> {
                        binding.imageView.setImageResource(R.drawable.__clear_sky_morning)
                    }
                    "03n"->
                    {
                        binding.imageView.setImageResource(R.drawable.__few_cloud_morning)
                    }
                    "04n"->
                    {
                        binding.imageView.setImageResource(R.drawable.__broken_cloud)
                    }

                }
            }else
            {
                when (currentItem.weather[0].icon) {
                    "01n" -> {
                        binding.imageView.setImageResource(R.drawable.__clear_sky_night)
                    }
                    "03n"->
                    {
                        binding.imageView.setImageResource(R.drawable.__few_clound_night)
                    }
                    "04n"->
                    {
                        binding.imageView.setImageResource(R.drawable.__broken_cloud)
                    }

                }
            }

        }

    }

}