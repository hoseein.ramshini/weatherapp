package com.example.hoseein_ramshin.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.hoseein_ramshin.R
import com.example.hoseein_ramshin.data.model.DayItem
import com.example.hoseein_ramshin.databinding.SimpleDayBinding
import com.example.hoseein_ramshin.util.Constants

class AdapterDay
    (private val listItem:List<DayItem>):RecyclerView.Adapter<AdapterDay.DayViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayViewHolder {

        val binding=SimpleDayBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return DayViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DayViewHolder, position: Int) {
        holder.bind(listItem[position])
    }

    override fun getItemCount(): Int {
       return listItem.size
    }
    inner class DayViewHolder(private val binding:SimpleDayBinding):RecyclerView.ViewHolder(binding.root)
    {
        @SuppressLint("SetTextI18n")
        fun bind(item:DayItem)
        {
            binding.textView5.text=item.name
            val max = item.item.main.temp_max - 273.15
            val min = item.item.main.temp_min - 273.15
            binding.temp.text="${min.toInt()}°/${max.toInt()}°"


            if (Constants.THEME_MODE !="Dark")
            {
                when (item.item.weather[0].icon) {
                    "01n" -> {
                        binding.imageView3.setImageResource(R.drawable.__clear_sky_morning)
                    }
                    "03n"->
                    {
                        binding.imageView3.setImageResource(R.drawable.__scattered_clouds)
                    }
                    "02d"->
                    {
                        binding.imageView3.setImageResource(R.drawable.__few_cloud_morning)
                    }
                    "04n"->
                    {
                        binding.imageView3.setImageResource(R.drawable.__broken_cloud)
                    }

                }
            }else
            {
                when (item.item.weather[0].icon) {
                    "01n" -> {
                        binding.imageView3.setImageResource(R.drawable.__clear_sky_night)
                    }
                    "02d"->
                    {
                        binding.imageView3.setImageResource(R.drawable.__few_clound_night)
                    }
                    "03n"->
                    {
                        binding.imageView3.setImageResource(R.drawable.__scattered_clouds)
                    }
                    "04n"->
                    {
                        binding.imageView3.setImageResource(R.drawable.__broken_cloud)
                    }

                }
            }
        }
    }
}