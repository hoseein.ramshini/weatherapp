package com.example.hoseein_ramshin.util


import android.annotation.SuppressLint
import com.example.hoseein_ramshin.BuildConfig
import java.text.SimpleDateFormat
@SuppressLint("SimpleDateFormat")
object Constants {

    const val BASE_URL: String="https://api.openweathermap.org/data/2.5/"
    const val TIME_OUT: Long=2000
    var THEME_MODE=""
    val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val DATE_FORMAT2 = SimpleDateFormat("yyyy-MM-dd")
    const val API_KEY=BuildConfig.API_KRY
    const val NESHAN_KEY=BuildConfig.NESHAN_KEY
}