package com.example.hoseein_ramshin.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.hoseein_ramshin.R
import com.example.hoseein_ramshin.data.model.ResponseValue
import com.example.hoseein_ramshin.data.model.SearchItem
import com.example.hoseein_ramshin.data.remote.NetworkResult
import com.example.hoseein_ramshin.repository.WeatherRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val weatherRepository: WeatherRepository) :
    ViewModel() {



    val forecastValue = MutableLiveData<NetworkResult<ResponseValue>>()

     var searchItem = SearchItem(
         lat =35.487265,
         lon = 51.696791,
         title = ""
     )
    fun forecast() {
        Log.e("7180", searchItem.toString() )
        viewModelScope.launch {
            forecastValue.value= weatherRepository.forecast(searchItem.lat,searchItem.lon)
        }
    }
}
