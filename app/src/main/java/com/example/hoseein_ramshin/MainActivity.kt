package com.example.hoseein_ramshin

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import com.example.hoseein_ramshin.databinding.ActivityMainBinding
import com.example.hoseein_ramshin.util.Constants.THEME_MODE
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.Calendar

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var job: Job


    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setViewTheme()

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val share = getSharedPreferences("Theme", Context.MODE_PRIVATE)
        job = lifecycleScope.launch {
            while (true) {
                delay(1000)
                val calendar = Calendar.getInstance()
                 // Log.e("7180", "onCreate: ${share.getString("Theme", null)}")
                if (calendar.get(Calendar.HOUR_OF_DAY) in 7..12 && share.getString(
                        "Theme",
                        null
                    ) != "Dark"
                ) {
                    val editor = share.edit()
                    editor.putString("Theme", "Dark")
                    editor.apply()
                    this@MainActivity.setTheme(R.style.Dark)
                    recreate()
                } else if (calendar.get(Calendar.HOUR_OF_DAY) in 13..19 && share.getString(
                        "Theme",
                        null
                    ) != "Light"
                ) {
                    val editor = share.edit()
                    editor.putString("Theme", "Light")
                    editor.apply()
                    this@MainActivity.setTheme(R.style.Day)
                    recreate()
                }


            }

        }
    }

    override fun onStop() {
        super.onStop()
        job.cancel()
    }

    private fun setViewTheme() {
        val share = getSharedPreferences("Theme", Context.MODE_PRIVATE)

        if (share.getString("Theme", null).isNullOrBlank() || share.getString("Theme", null)
                .equals("Dark")
        ) {
            THEME_MODE="Light"
            this@MainActivity.setTheme(R.style.Day)
        }else {
            this@MainActivity.setTheme(R.style.Dark)
            THEME_MODE="Dark"
        }

    }
}