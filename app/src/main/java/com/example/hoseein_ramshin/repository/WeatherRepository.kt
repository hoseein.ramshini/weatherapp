package com.example.hoseein_ramshin.repository

import com.example.hoseein_ramshin.data.model.ResponseValue
import com.example.hoseein_ramshin.data.remote.ApiService
import com.example.hoseein_ramshin.data.remote.BaseApiResponse
import com.example.hoseein_ramshin.data.remote.NetworkResult
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WeatherRepository @Inject constructor(private val apiService: ApiService) :
    BaseApiResponse() {


    suspend fun forecast(lat: Double, lon: Double): NetworkResult<ResponseValue> =
        safeApiCall {
            apiService.forecast(lat, lon)
        }

}