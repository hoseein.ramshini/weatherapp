package com.example.hoseein_ramshin.data.model

data class SearchItem(
    val title:String,
    val lat:Double,
    val lon:Double,
)
