package com.example.hoseein_ramshin.data.model

data class DayItem (
    val name:String,
    val item:ListItem
)