package com.example.hoseein_ramshin.data.model

data class Coord(
    val lat: Double,
    val lon: Double
)