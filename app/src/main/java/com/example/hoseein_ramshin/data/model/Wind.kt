package com.example.hoseein_ramshin.data.model

data class Wind(
    val deg: Int,
    val gust: Double,
    val speed: Double
)