package com.example.hoseein_ramshin.data.model

data class ResponseValue(
    val city: City,
    val cnt: Int,
    val cod: String,
    val list: List<ListItem>,
    val message: Int
)