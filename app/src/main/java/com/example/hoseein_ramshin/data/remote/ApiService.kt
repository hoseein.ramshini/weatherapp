package com.example.hoseein_ramshin.data.remote

import com.example.hoseein_ramshin.data.model.ResponseValue
import com.example.hoseein_ramshin.util.Constants
import retrofit2.Response
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiService {


    @POST("forecast")
    suspend fun forecast(
        @Query("lat") lat: Double,
        @Query("lon")lon: Double,
        @Query("appid") string: String=Constants.API_KEY
    ):Response<ResponseValue>

}