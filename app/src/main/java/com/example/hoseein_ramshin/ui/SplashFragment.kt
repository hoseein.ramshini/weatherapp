package com.example.hoseein_ramshin.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.hoseein_ramshin.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.Timer
import java.util.TimerTask

@AndroidEntryPoint
class SplashFragment :Fragment(R.layout.fragment_splash) {

    @OptIn(DelicateCoroutinesApi::class)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Timer().schedule(object :TimerTask(){

            override fun run() {
            }
        },3000)


        GlobalScope.launch(Dispatchers.Main) {
            delay(3000)
            val action=SplashFragmentDirections.actionSplashFragmentToFirstFragment()
            findNavController().navigate(action)
        }

    }
}