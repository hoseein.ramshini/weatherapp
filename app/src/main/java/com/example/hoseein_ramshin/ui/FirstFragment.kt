package com.example.hoseein_ramshin.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.hoseein_ramshin.R
import com.example.hoseein_ramshin.adapter.AdapterDay
import com.example.hoseein_ramshin.adapter.AdapterTimes
import com.example.hoseein_ramshin.data.model.DayItem
import com.example.hoseein_ramshin.data.remote.NetworkResult
import com.example.hoseein_ramshin.databinding.FirstFragmentBinding
import com.example.hoseein_ramshin.util.Constants
import com.example.hoseein_ramshin.util.Constants.DATE_FORMAT2
import com.example.hoseein_ramshin.util.Constants.THEME_MODE
import com.example.hoseein_ramshin.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.time.DayOfWeek
import java.util.Calendar
import java.util.Date


@AndroidEntryPoint
@SuppressLint("SimpleDateFormat", "SetTextI18n")
class FirstFragment : Fragment(R.layout.first_fragment) {

    private val viewModel: HomeViewModel by activityViewModels()

    private lateinit var binding: FirstFragmentBinding
    private var isLoading: Boolean = true


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FirstFragmentBinding.bind(view)



        viewModel.forecast()




        viewModel.forecastValue.observe(viewLifecycleOwner)
        {

            when (it) {
                is NetworkResult.Success -> {
                    isLoading = false
                    it.data?.let { data ->
                        binding.cityName.text = data.city.name
                        binding.listItem.apply {
                            adapter = AdapterTimes(data.list.subList(0, 8))
                            layoutManager = LinearLayoutManager(
                                requireActivity(),
                                LinearLayoutManager.HORIZONTAL,
                                false
                            )
                        }
                        val currentDay = data.list.subList(0, 8)
                        val temp = currentDay[0].main.temp - 273.15
                        val max = currentDay[0].main.temp_max - 273.15
                        val min = currentDay[0].main.temp_min - 273.15
                        binding.textView4.text="${min.toInt()}°/${max.toInt()}°"
                        binding.textView3.text = "${temp.toInt()}°"
                        if (THEME_MODE !="Dark")
                        {
                            when (currentDay[0].weather[0].icon) {
                                "01d" -> {
                                    binding.imageView2.setImageResource(R.drawable.__clear_sky_morning)
                                }
                                "03d"->
                                {
                                    binding.imageView2.setImageResource(R.drawable.__scattered_clouds)
                                }
                                "02d"->
                                {
                                    binding.imageView2.setImageResource(R.drawable.__few_cloud_morning)
                                }
                                "04d"->
                                {
                                    binding.imageView2.setImageResource(R.drawable.__broken_cloud)
                                }

                            }
                        }
                        else
                        {
                            when (currentDay[0].weather[0].icon) {
                                "01n" -> {
                                    binding.imageView2.setImageResource(R.drawable.__clear_sky_night)
                                }

                                "02n" -> {
                                    binding.imageView2.setImageResource(R.drawable.__few_clound_night)
                                }

                                "03n" -> {
                                    binding.imageView2.setImageResource(R.drawable.__scattered_clouds)
                                }

                                "04n" -> {
                                    binding.imageView2.setImageResource(R.drawable.__broken_cloud)
                                }

                            }
                        }







                        val dayOne = data.list.subList(8, 16)



                        val twoOne = data.list.subList(16, 24)


                        val thereOne = data.list.subList(24, 32)


                        val fourOne = data.list.subList(32, 40)

                        val dates = ArrayList<Date>()
                        for (item in it.data.list.subList(8, 40)) {
                            val date = DATE_FORMAT2.parse(item.dt_txt)
                            if (date != null) {
                                if (date !in dates&&date>Date())
                                    dates.add(date)
                            }
                        }


                        val days=ArrayList<DayItem>()
                        days.add(DayItem(name=getDayName(dates[0]), item = dayOne[0]))
                        days.add(DayItem(name=getDayName(dates[1]), item = twoOne[1]))
                        days.add(DayItem(name=getDayName(dates[2]), item = thereOne[2]))
                        days.add(DayItem(name=getDayName(dates[3]), item = fourOne[3]))

                        binding.dayList.apply {
                            adapter=AdapterDay(days)
                            layoutManager=LinearLayoutManager(requireContext())
                        }


                    }


                }

                is NetworkResult.Error -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                    isLoading = false
                }

                is NetworkResult.Loading -> {

                    isLoading = true
                }
            }

        }
        binding.cityName.setOnClickListener {
            if (!isLoading) {
                val action = FirstFragmentDirections.actionFirstFragmentToSelectLocationFragment()
                findNavController().navigate(action)
            }
        }


    }


    private fun getDayName(date: Date): String {
        val calendar = Calendar.getInstance()
        calendar.time = date
        when (calendar.get(Calendar.DAY_OF_WEEK)) {
            7 -> return "Saturday"
            1 -> return "Sunday"
            2 -> return "Monday"
            3 -> return "Thursday"
            4 -> return "Wednesday"
            5 -> return "Thursday"
            6 -> return "Friday"
        }
        return ""

    }
}