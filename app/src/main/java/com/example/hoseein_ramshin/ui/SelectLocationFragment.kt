package com.example.hoseein_ramshin.ui

import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.carto.styles.MarkerStyleBuilder
import com.carto.utils.BitmapUtils
import com.example.hoseein_ramshin.R
import com.example.hoseein_ramshin.adapter.AdapterSearch
import com.example.hoseein_ramshin.data.model.SearchItem
import com.example.hoseein_ramshin.databinding.FragmentMapBinding
import com.example.hoseein_ramshin.util.Constants.NESHAN_KEY
import com.example.hoseein_ramshin.viewmodel.HomeViewModel
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.google.android.gms.location.Priority
import com.google.android.gms.location.SettingsClient
import com.google.android.gms.tasks.OnSuccessListener
import dagger.hilt.android.AndroidEntryPoint
import org.neshan.common.model.LatLng
import org.neshan.mapsdk.MapView
import org.neshan.mapsdk.model.Marker
import org.neshan.servicessdk.search.NeshanSearch
import org.neshan.servicessdk.search.model.NeshanSearchResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DateFormat
import java.util.ArrayList
import java.util.Date


class SelectLocationFragment : Fragment(R.layout.fragment_map),AdapterSearch.OnItemClick {
    private lateinit var mMap: MapView
private val viewModel : HomeViewModel by activityViewModels()


    private lateinit var binding: FragmentMapBinding
    private lateinit var currentLocation: Location
    private lateinit var selectedLocation: LatLng
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val permissionCode = 101
    private val REQUEST_CODE = 123


    private var marker: Marker? = null
    private var userLocation: Location? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var mRequestingLocationUpdates: Boolean? = null
    private lateinit var settingsClient: SettingsClient
    private lateinit var locationRequest: LocationRequest
    private var locationSettingsRequest: LocationSettingsRequest? = null
    private var locationCallback: LocationCallback? = null
    private var lastUpdateTime: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMapBinding.bind(view)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity())


        mMap=view.findViewById(R.id.mapview)

        //mMap.myLocationEnabled=true
        mMap.setOnCameraIdleListener {
            val center = mMap.cameraTargetPosition
            val sLatitude = String.format("%.6f", center.latitude)
            val sLongitude = String.format("%.6f", center.longitude)
            val mLatLng = StringBuilder()
            mLatLng.append(sLatitude)
            mLatLng.append("°")
            mLatLng.append(" ")
            mLatLng.append(sLongitude)
            mLatLng.append("°")
            selectedLocation= LatLng(center.latitude,center.longitude)
        }

        binding.search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                search(s.toString())

            }
        })
       /* binding.currentLocation.setOnClickListener {
            if (userLocation != null) {
                mMap.moveCamera(
                    org.neshan.common.model.LatLng(
                        userLocation!!.latitude,
                        userLocation!!.longitude
                    ), 0.25f
                )
                mMap.setZoom(18f, 0.25f)
            } else {
                startReceivingLocationUpdates()
            }
        }*/


       /* binding.selectedLocation.setOnClickListener {
            val customerAddress= CustomerAddress(
                lat = selectedLocation.latitude,
                lon = selectedLocation.longitude
            )
            val action=SelectLocationFragmentDirections.actionSelectLocationFragmentToAddNewAddressFragment(customerAddress)
            findNavController().navigate(action)
        }*/
    }

    private fun search(term: String) {
        val searchPosition: LatLng = mMap.cameraTargetPosition
        //updateCenterMarker(searchPosition)
        NeshanSearch.Builder(NESHAN_KEY)
            .setLocation(searchPosition)
            .setTerm(term)
            .build().call(object : Callback<NeshanSearchResult?> {
                override fun onResponse(
                    call: Call<NeshanSearchResult?>,
                    response: Response<NeshanSearchResult?>
                ) {
                    if (response.code() == 403) {
                        Toast.makeText(
                            context,
                            "کلید دسترسی نامعتبر",
                            Toast.LENGTH_LONG
                        ).show()
                        return
                    }
                    if (response.body() != null) {
                        val result = response.body()

                       val items= ArrayList<SearchItem>()
                       for (item in result!!.items)
                       {
                           items.add(
                               SearchItem(item.title,
                                item.location.latitude,
                               item.location.longitude)
                           )

                       }

                        val adapter=AdapterSearch(items,this@SelectLocationFragment)
                        binding.searchItems.adapter=adapter
                        binding.searchItems.layoutManager=LinearLayoutManager(requireActivity())




                        //items = result!!.items
                        //adapter.updateList(items)
                    }
                }

                override fun onFailure(call: Call<NeshanSearchResult?>, t: Throwable) {
                    Log.i("7180", "onFailure: " + t.message)
                    Toast.makeText(context, "ارتباط برقرار نشد!", Toast.LENGTH_SHORT)
                        .show()
                }
            })
    }

    override fun itemClick(searchItem: SearchItem) {

        viewModel.searchItem=searchItem
        findNavController().popBackStack()

    }
}