pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        //Add Neshan maven repo here
        maven ("https://maven.neshan.org/artifactory/public-maven")
    }
}

rootProject.name = "Hoseein_Ramshin"
include(":app")
 